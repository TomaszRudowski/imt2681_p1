# README #

Repository for IMT2651 project 1

## Localhost ##
version from the first commit runs on localhost and passed tests

## Deployment ##
version from the second commit (gcloud fit) runs at:
http://imt2681-project1.appspot.com

## Usage ##
accepts following url to fetch JSON data from github

###http://imt2681-project1.appspot.com/github.com/[owner]/[project]###
###http://imt2681-project1.appspot.com/github.com/[owner]/[project]/###
###http://imt2681-project1.appspot.com/[owner]/[project]###
###http://imt2681-project1.appspot.com/[owner]/[project]/###