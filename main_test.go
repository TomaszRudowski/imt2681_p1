package main

import (
	"testing"
	"net/http/httptest"
	"net/http"
)

/**
* testing given url in both correct and wrong form
* based on https://github.com/marni/imt2681_studentdb/blob/master/api_student_test.go
*/
func Test_givenUrl (t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(handler))
	defer ts.Close()

	testCases := []string{
		ts.URL + "/github.com/golang/go",
		ts.URL + "/github.com/golang/go/",
		ts.URL + "/golang/go",
		ts.URL + "/golang/go/",
	}
	for _, tstring := range testCases {
		resp, err := http.Get(tstring)
		if err != nil {
			t.Errorf("Error making the GET request, %s", err)
		}

		if resp.StatusCode == http.StatusBadRequest {
			t.Errorf("For route: %s, expected StatusCode %d, received %d", tstring,
				http.StatusBadRequest, resp.StatusCode)
			return
		}
	}
	testCasesErr := []string{
		ts.URL,
		ts.URL + "/githubXXX.com/golang/go",
		ts.URL + "/github.com/golang/go/XXX",
	}
	for _, tstring := range testCasesErr {
		resp, err := http.Get(tstring)
		if err != nil {
			t.Errorf("Error making the GET request, %s", err)
		}

		if resp.StatusCode != http.StatusBadRequest {
			t.Errorf("For route: %s, expected StatusCode %d, received %d", tstring,
				http.StatusBadRequest, resp.StatusCode)
			return
		}
	}


}
/*
func Test_handlerStudent_malformedURL(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(handlerStudent))
	defer ts.Close()

	testCases := []string{
		ts.URL,
		ts.URL + "/student/id/extra",
		ts.URL + "/stud/",
	}
	for _, tstring := range testCases {
		resp, err := http.Get(tstring)
		if err != nil {
			t.Errorf("Error making the GET request, %s", err)
		}

		if resp.StatusCode != http.StatusBadRequest {
			t.Errorf("For route: %s, expected StatusCode %d, received %d", tstring,
				http.StatusBadRequest, resp.StatusCode)
			return
		}
	}
}*/