package main

import (
	"net/http"
	"fmt"
	"encoding/json"
	"strings"
	//"log"
	//"io/ioutil"
	"google.golang.org/appengine/urlfetch"
	"google.golang.org/appengine"
	//"context"
	//"context"
	"io/ioutil"
)

var dummy_json_committers = `[
  {
    "login": "rsc",
    "id": 104030,
    "avatar_url": "https://avatars1.githubusercontent.com/u/104030?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/rsc",
    "html_url": "https://github.com/rsc",
    "followers_url": "https://api.github.com/users/rsc/followers",
    "following_url": "https://api.github.com/users/rsc/following{/other_user}",
    "gists_url": "https://api.github.com/users/rsc/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/rsc/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/rsc/subscriptions",
    "organizations_url": "https://api.github.com/users/rsc/orgs",
    "repos_url": "https://api.github.com/users/rsc/repos",
    "events_url": "https://api.github.com/users/rsc/events{/privacy}",
    "received_events_url": "https://api.github.com/users/rsc/received_events",
    "type": "User",
    "site_admin": false,
    "contributions": 5996
  },
  {
    "login": "robpike",
    "id": 4324516,
    "avatar_url": "https://avatars0.githubusercontent.com/u/4324516?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/robpike",
    "html_url": "https://github.com/robpike",
    "followers_url": "https://api.github.com/users/robpike/followers",
    "following_url": "https://api.github.com/users/robpike/following{/other_user}",
    "gists_url": "https://api.github.com/users/robpike/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/robpike/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/robpike/subscriptions",
    "organizations_url": "https://api.github.com/users/robpike/orgs",
    "repos_url": "https://api.github.com/users/robpike/repos",
    "events_url": "https://api.github.com/users/robpike/events{/privacy}",
    "received_events_url": "https://api.github.com/users/robpike/received_events",
    "type": "User",
    "site_admin": false,
    "contributions": 2904
  }
]`

var dummy_json_lang = `{
"Go": 36771047,
"Assembly": 2627015,
"HTML": 2336622,
"C": 193250,
"Shell": 58350,
"Perl": 34799,
"Python": 13239,
"Batchfile": 7351,
"JavaScript": 2550,
"Protocol Buffer": 2075,
"C++": 1370,
"Logos": 1248,
"Makefile": 748,
"Awk": 450,
"Fortran": 394,
"CSS": 8
}`

var dummy_json = `{
		"id": 23096959,
			"name": "go",
			"full_name": "golang/go",
			"owner": {
		"login": "golang",
		"id": 4314092,
		"avatar_url": "https://avatars3.githubusercontent.com/u/4314092?v=4",
		"gravatar_id": "",
		"url": "https://api.github.com/users/golang",
		"html_url": "https://github.com/golang",
		"followers_url": "https://api.github.com/users/golang/followers",
		"following_url": "https://api.github.com/users/golang/following{/other_user}",
		"gists_url": "https://api.github.com/users/golang/gists{/gist_id}",
		"starred_url": "https://api.github.com/users/golang/starred{/owner}{/repo}",
		"subscriptions_url": "https://api.github.com/users/golang/subscriptions",
		"organizations_url": "https://api.github.com/users/golang/orgs",
		"repos_url": "https://api.github.com/users/golang/repos",
		"events_url": "https://api.github.com/users/golang/events{/privacy}",
		"received_events_url": "https://api.github.com/users/golang/received_events",
		"type": "Organization",
		"site_admin": false
		},
		"private": false,
			"html_url": "https://github.com/golang/go",
			"description": "The Go programming language",
			"fork": false,
			"url": "https://api.github.com/repos/golang/go",
			"forks_url": "https://api.github.com/repos/golang/go/forks",
			"keys_url": "https://api.github.com/repos/golang/go/keys{/key_id}",
			"collaborators_url": "https://api.github.com/repos/golang/go/collaborators{/collaborator}",
			"teams_url": "https://api.github.com/repos/golang/go/teams",
			"hooks_url": "https://api.github.com/repos/golang/go/hooks",
			"issue_events_url": "https://api.github.com/repos/golang/go/issues/events{/number}",
			"events_url": "https://api.github.com/repos/golang/go/events",
			"assignees_url": "https://api.github.com/repos/golang/go/assignees{/user}",
			"branches_url": "https://api.github.com/repos/golang/go/branches{/branch}",
			"tags_url": "https://api.github.com/repos/golang/go/tags",
			"blobs_url": "https://api.github.com/repos/golang/go/git/blobs{/sha}",
			"git_tags_url": "https://api.github.com/repos/golang/go/git/tags{/sha}",
			"git_refs_url": "https://api.github.com/repos/golang/go/git/refs{/sha}",
			"trees_url": "https://api.github.com/repos/golang/go/git/trees{/sha}",
			"statuses_url": "https://api.github.com/repos/golang/go/statuses/{sha}",
			"languages_url": "https://api.github.com/repos/golang/go/languages",
			"stargazers_url": "https://api.github.com/repos/golang/go/stargazers",
			"contributors_url": "https://api.github.com/repos/golang/go/contributors",
			"subscribers_url": "https://api.github.com/repos/golang/go/subscribers",
			"subscription_url": "https://api.github.com/repos/golang/go/subscription",
			"commits_url": "https://api.github.com/repos/golang/go/commits{/sha}",
			"git_commits_url": "https://api.github.com/repos/golang/go/git/commits{/sha}",
			"comments_url": "https://api.github.com/repos/golang/go/comments{/number}",
			"issue_comment_url": "https://api.github.com/repos/golang/go/issues/comments{/number}",
			"contents_url": "https://api.github.com/repos/golang/go/contents/{+path}",
			"compare_url": "https://api.github.com/repos/golang/go/compare/{base}...{head}",
			"merges_url": "https://api.github.com/repos/golang/go/merges",
			"archive_url": "https://api.github.com/repos/golang/go/{archive_format}{/ref}",
			"downloads_url": "https://api.github.com/repos/golang/go/downloads",
			"issues_url": "https://api.github.com/repos/golang/go/issues{/number}",
			"pulls_url": "https://api.github.com/repos/golang/go/pulls{/number}",
			"milestones_url": "https://api.github.com/repos/golang/go/milestones{/number}",
			"notifications_url": "https://api.github.com/repos/golang/go/notifications{?since,all,participating}",
			"labels_url": "https://api.github.com/repos/golang/go/labels{/name}",
			"releases_url": "https://api.github.com/repos/golang/go/releases{/id}",
			"deployments_url": "https://api.github.com/repos/golang/go/deployments",
			"created_at": "2014-08-19T04:33:40Z",
			"updated_at": "2017-09-18T06:51:00Z",
			"pushed_at": "2017-09-18T00:33:47Z",
			"git_url": "git://github.com/golang/go.git",
			"ssh_url": "git@github.com:golang/go.git",
			"clone_url": "https://github.com/golang/go.git",
			"svn_url": "https://github.com/golang/go",
			"homepage": "https://golang.org",
			"size": 159448,
			"stargazers_count": 32107,
			"watchers_count": 32107,
			"language": "Go",
			"has_issues": true,
			"has_projects": false,
			"has_downloads": true,
			"has_wiki": true,
			"has_pages": false,
			"forks_count": 4354,
			"mirror_url": null,
			"open_issues_count": 3144,
			"forks": 4354,
			"open_issues": 3144,
			"watchers": 32107,
			"default_branch": "master",
			"organization": {
		"login": "golang",
		"id": 4314092,
		"avatar_url": "https://avatars3.githubusercontent.com/u/4314092?v=4",
		"gravatar_id": "",
		"url": "https://api.github.com/users/golang",
		"html_url": "https://github.com/golang",
		"followers_url": "https://api.github.com/users/golang/followers",
		"following_url": "https://api.github.com/users/golang/following{/other_user}",
		"gists_url": "https://api.github.com/users/golang/gists{/gist_id}",
		"starred_url": "https://api.github.com/users/golang/starred{/owner}{/repo}",
		"subscriptions_url": "https://api.github.com/users/golang/subscriptions",
		"organizations_url": "https://api.github.com/users/golang/orgs",
		"repos_url": "https://api.github.com/users/golang/repos",
		"events_url": "https://api.github.com/users/golang/events{/privacy}",
		"received_events_url": "https://api.github.com/users/golang/received_events",
		"type": "Organization",
		"site_admin": false
		},
		"network_count": 4354,
			"subscribers_count": 2439
	}`

type testJSONcommitters struct {
	Login string `json:"login"`
	Contributions int `json:"contributions"`
}
type testJSON struct {
	RawData map[string]interface{}
}

type OwnerSTR struct {
	Login string	`json:"login"`
}

type JSON_resp struct {
	Full_name string 	`json:"full_name"`
	Owner OwnerSTR `json:"owner"`
}

type finalData struct {
	Project string `json:"project"`
	Owner string `json:"owner"`
	Committer string `json:"committer"`
	Commits int `json:"commits"`
	Languages []string `json:"languages"`
}

var finalObject finalData	// global object to store data to be parsed to JSON

/**
* Support function for all FetchJSON functions sends request and collect data
* @param string url to be used in request
* @return JSON as a byte array to be parsed
 */
func sendNewRequest (urlPath string) []byte {

	// based on https://cloud.google.com/appengine/docs/standard/go/issue-requests
	c := appengine.NewContext(rr)
	client := urlfetch.Client(c)
	res, err := client.Get(urlPath)
	if err != nil {
		http.Error(ww, err.Error(), http.StatusInternalServerError)
		//log.Fatal(err)	// log. problems I couldn't solve
	}
	JSONdata, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		http.Error(ww, err.Error(), http.StatusInternalServerError)
		//log.Fatal(err)   // log. problems I couldn't solve
	}

	return JSONdata
}

/**
* Parses []byte from support function into global object data
* @param strings to be used in url path to github api
 */
func fetchMainJSON (owner, project string) {
	var resultMain JSON_resp

	// using dummy data !!!
//	dummy_json_bytes_main := []byte(dummy_json)
//	json.Unmarshal(dummy_json_bytes_main,&resultMain)

	// using real data !!! working
	reqUrl := "https://api.github.com/repos/" + owner + "/" + project
	json.Unmarshal(sendNewRequest(reqUrl), &resultMain)

	finalObject.Project = "github.com/" + resultMain.Full_name // string formatting
	finalObject.Owner = resultMain.Owner.Login
}

/**
* Parses []byte from support function into global object data
* @param strings to be used in url path to github api
 */
func fetchCommitterJSON (owner, project string) {
	resultCommitters := new([1]testJSONcommitters)

	// using dummy data !!!
//	dummy_json_bytes_committers := []byte(dummy_json_committers)
//	json.Unmarshal(dummy_json_bytes_committers,&resultCommitters)

	// using real data !!! working
	reqUrl := "https://api.github.com/repos/" + owner + "/" + project + "/contributors"
	json.Unmarshal(sendNewRequest(reqUrl), &resultCommitters)

	finalObject.Committer = resultCommitters[0].Login
	finalObject.Commits = resultCommitters[0].Contributions
}

/**
* Parses []byte from support function into global object data
* @param strings to be used in url path to github api
 */
func fetchLanguagesJSON (owner, project string) {
	var result2 testJSON
	languages := new(map[string]interface{})

	// using dummy data !!!
//	dummy_json_bytes_lang := []byte(dummy_json_lang)
//	json.Unmarshal(dummy_json_bytes_lang,&languages)

	// using real data !!! working
 	reqUrl := "https://api.github.com/repos/" + owner + "/" + project + "/languages"
	json.Unmarshal(sendNewRequest(reqUrl), &languages)

	result2.RawData = *languages					// get data into temp struct

	//source: corrected during lecture
	a := make([]string, 0, len(result2.RawData))	//new empty array with enough capacity

	for k := range result2.RawData {
		a = append(a, k)
	}

	finalObject.Languages = a						// get final array
}

/**
* @param string url path already in string form
* @return strings "owner" and "project" names from url written in address line
 */
func extractStringsFromUrlPath (urlPath string) (string, string) {
	parts := strings.Split(urlPath, "/")

	switch len(parts) {
	case 3:
		// server/[owner]/[project]
		return parts[1], parts[2]
	case 4:
		// server/github.com/[owner]/[project] or !! server/[owner]/[project]/
		if len(parts[3]) > 0  {
			// consider: server/github.com/[owner]/[project]
			if strings.Compare("github.com", parts[1]) == 0 {
				return parts[2], parts[3]
			} else { // and server/[owner]/[project]/... which is fail
				return "", "error 1. Unable to extract owner and project name"
			}

		} else {
			return parts[1], parts[2]
		}
	case 5:
		// server/github.com/[owner]/[project]/ or wrong input: sth more then .../[project]/...
		if len(parts[4]) > 0  {
			return "", "error 2. Unable to extract owner and project name"
		} else {
			return parts[2], parts[3]
		}
	default:
		// error
		return "", "error 3. Unable to extract owner and project name"
	}

}

// those two I needed to work around gcloud get issues,
// i.e. I need access to them in sendNewRequest()
var ww http.ResponseWriter
var rr *http.Request

/**
* Main function to handle http.Request if not able to extract "owner" and "project"
* responses with error message, else fetching data from github api (alt. dummy data)
*/
func handler(w http.ResponseWriter, r *http.Request) {
	ww = w
	rr = r
	//c = appengine.NewContext(r)	// moved to sendNewRequest()
	owner, project := extractStringsFromUrlPath(r.URL.Path)
	if len(owner) == 0 {			// function extractStringsFromURL failed
		errorMsg := project			// the second of returned strings contains error message
		http.Error(w, errorMsg,400)	// reconsider code? 400 used in tests
		return
	} else {
				// the first one probably not needed since we have already data,
		fetchMainJSON(owner, project)		// but stays in case future changes
		fetchCommitterJSON(owner, project)
		fetchLanguagesJSON(owner, project)
	}

	// https://kev.inburke.com/kevin/golang-json-http/

//	!! working final, add error handling for Marshal()
	out, _ := json.Marshal(finalObject)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%s", out)

}

/**
* Main defines handling function for all url at (given listening server) -> not on gcloud
* main() called init() on gcloud
 */
func init() {

	http.HandleFunc("/", handler)
	//http.ListenAndServe("127.0.0.1:8080", nil)  // not needed on gcloud
}
